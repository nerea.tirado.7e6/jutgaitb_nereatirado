/**
 * Este trabajo troncal de la unidad formativa sobre el "JutjeITB" trata sobre una serie de problemas a resolver al usuario
 * @author Nerea Tirado Cadenas
 * @version Prueba 1, 2, 3 y 4 es del dia 21/02/2023
 */

class Problema(t: String, inPub: String, outPub: Int, inPriv: String, outPriv: Int){
    var texto = t
    var inputPublic = inPub
    var inputPrivat = inPriv
    var outputPublic = outPub
    var outPrivat = outPriv
    var solucion = false
    var intentos = 0
    var listaIntentos = ""
    var jugarDeNuevo = true
}
/**
 ** Esta es la función principal para crear nuestro programa donde paso a paso os explico como lo he resuelto:
 * 1. Hemos dado la bienvenida a nuestro usuario
 * 2. Hemos creado cinco variables con diferentes enunciados que vamos pasando todos los atributos que hemos dado a class
 * 3. Mediante do, if, else y while hemos seguido ese patrón cinco veces conde en ellos nos muestra un ejemplo con inputs y outpus públicos
 * una idea de lo que ha de seguir el usuario
 * 4. Mostrará una pregunta donde el usuario deberá mostrar una respuesta sin poder ver el outputPrivado que guarda el usuario
 * 5. El usuario podrá volver a poner números hasta que lo adivine porque no tiene un máximo de intentos
 * 6. Habrá unas estadísticas donde mostrara los intentos que a utilizado en cada problema
 * 7. Otra estadística con el total de intentos realizados
 * 8. Habrá una estadísticas donde mostrara todos los números que utiliza el usuario hasta ser acertado
 * 9. Pregunatara al usuario si quiere resolverlo o no
 * @author Nerea Tirado Cadenas
 * @version Prueba 1, 2, 3 y 4 es del dia 21/02/2023
 */
fun main(args: Array<String>) {
    println("Bienvenido a nuestro programa")
    println("#############################")
    println("Empezamos con los problemas: ")

    //todas las variables de los problemas
    val enunciado1 = Problema("Problema 1", "2 y 2", 4, "7 y 3", 10)
    val enunciado2 = Problema("Problema 2", "3 y 3", 9, "8 y 4", 32)
    val enunciado3 = Problema("Problema 3", "16 y 2", 8, "9 y 3", 3)
    val enunciado4 = Problema("Problema 4", "21, 48, 50", 3450, "19, 18, 7", 145)
    val enunciado5 = Problema("Problema 5", "100, 453", 45300, "28 y 40", 1120)


    //PROBLEMA 1
    println("Problema 1: ")
    println(enunciado1.inputPublic)
    println(enunciado1.outputPublic)
    println("Estos son los dos numeros a calcular puedes sumar, retsar, dividir o multiplicar:  ")
    println(enunciado1.inputPrivat)
    println("Quieres resolverlo? (s/n)")
    var respuesta = readln()
    if (respuesta != null && respuesta.lowercase() == "n"){
        enunciado1.jugarDeNuevo=false
    } else if (respuesta != null && respuesta.lowercase() == "s"){
        enunciado1.jugarDeNuevo=true
    }
    if (enunciado1.jugarDeNuevo==true){
        do {
            println("Introduce el resultado: ")
            var resultadoUsuario = readln().toInt()
            if (enunciado1.outPrivat == resultadoUsuario) {
                println("ACERTASTE!!")
                enunciado1.solucion = true
            } else {
                enunciado1.listaIntentos += resultadoUsuario.toString() + "/"
                println("Fallaste vuelve hacerlo de nuevo")
                println("Has intentado estos numeros: ${enunciado1.listaIntentos}")
            }
            enunciado1.intentos++
        } while (enunciado1.solucion == false)
    } else println("Pasamos al siguiente")

    //PROBLEMA 2
    println("Problema 2: ")
    println(enunciado2.inputPublic)
    println(enunciado2.outputPublic)
    println("Estos son los dos numeros a calcular puedes sumar, retsar, dividir o multiplicar:  ")
    println(enunciado2.inputPrivat)
    println("Quieres resolverlo? (s/n)")
    respuesta = readln()
    if (respuesta != null && respuesta.lowercase() == "n"){
        enunciado2.jugarDeNuevo=false
    } else if (respuesta != null && respuesta.lowercase() == "s"){
        enunciado2.jugarDeNuevo=true
    }
    if (enunciado2.jugarDeNuevo==true){
        do {
            println("Introduce el resultado: ")
            var resultadoUsuario = readln().toInt()
            if (enunciado2.outPrivat == resultadoUsuario) {
                println("ACERTASTE!!")
                enunciado2.solucion = true
            } else {
                enunciado2.listaIntentos += resultadoUsuario.toString() + "/"
                println("Fallaste vuelve hacerlo de nuevo")
                println("Has intentado estos numeros: ${enunciado2.listaIntentos}")
            }
            enunciado2.intentos++
        } while (enunciado2.solucion == false)
    } else println("Pasamos al siguiente")

    //PROBLEMA 3
    println("Problema 3: ")
    println(enunciado3.inputPublic)
    println(enunciado3.outputPublic)
    println("Estos son los dos numeros a calcular puedes sumar, restar, dividir o multiplicar:  ")
    println(enunciado3.inputPrivat)
    println("Quieres resolverlo? (s/n)")
    respuesta = readln()
    if (respuesta != null && respuesta.lowercase() == "n"){
        enunciado3.jugarDeNuevo=false
    } else if (respuesta != null && respuesta.lowercase() == "s"){
        enunciado3.jugarDeNuevo=true
    }
    if (enunciado3.jugarDeNuevo==true){
        do {
            println("Introduce el resultado: ")
            var resultadoUsuario = readln().toInt()
            if (enunciado3.outPrivat == resultadoUsuario) {
                println("ACERTASTE!!")
                enunciado3.solucion = true
            } else {
                enunciado3.listaIntentos += resultadoUsuario.toString() + "/"
                println("Fallaste vuelve hacerlo de nuevo")
                println("Has intentado estos numeros: ${enunciado3.listaIntentos}")
            }
            enunciado3.intentos++
        } while (enunciado3.solucion == false)
    } else println("Pasamos al siguiente")

    //PROBLEMA 4
    println("Problema 4: ")
    println(enunciado4.inputPublic)
    println(enunciado4.outputPublic)
    println("Estos son los dos numeros a calcular puedes sumar, retsar, dividir o multiplicar:  ")
    println(enunciado4.inputPrivat)
    println("Quieres resolverlo? (s/n)")
    respuesta = readln()
    if (respuesta != null && respuesta.lowercase() == "n"){
        enunciado3.jugarDeNuevo=false
    } else if (respuesta != null && respuesta.lowercase() == "s"){
        enunciado4.jugarDeNuevo=true
    }
    if (enunciado4.jugarDeNuevo==true){
        do {
            println("Introduce el resultado: ")
            var resultadoUsuario = readln().toInt()
            if (enunciado4.outPrivat == resultadoUsuario) {
                println("ACERTASTE!!")
                enunciado4.solucion = true
            } else {
                enunciado4.listaIntentos += resultadoUsuario.toString() + "/"
                println("Fallaste vuelve hacerlo de nuevo")
                println("Has intentado estos numeros: ${enunciado4.listaIntentos}")
            }
            enunciado4.intentos++
        } while (enunciado4.solucion == false)
    } else println("Pasamos al siguiente")

    //PROBLEMA 5
    println("Problema 5: ")
    println(enunciado5.inputPublic)
    println(enunciado5.outputPublic)
    println("Estos son los dos numeros a calcular puedes sumar, retsar, dividir o multiplicar:  ")
    println(enunciado5.inputPrivat)
    println("Quieres resolverlo? (s/n)")
    respuesta = readln()
    if (respuesta != null && respuesta.lowercase() == "n"){
        enunciado5.jugarDeNuevo=false
    } else if (respuesta != null && respuesta.lowercase() == "s"){
        enunciado5.jugarDeNuevo=true
    }
    if (enunciado5.jugarDeNuevo==true){
        do {
            println("Introduce el resultado: ")
            var resultadoUsuario = readln().toInt()
            if (enunciado5.outPrivat == resultadoUsuario) {
                println("ACERTASTE!!")
                enunciado5.solucion = true
            } else {
                enunciado5.listaIntentos += resultadoUsuario.toString() + "/"
                println("Fallaste vuelve hacerlo de nuevo")
                println("Has intentado estos numeros: ${enunciado5.listaIntentos}")
            }
            enunciado5.intentos++
        } while (enunciado5.solucion == false)
    } else println("Pasamos al siguiente")

    //ESTADISTICAS
    println("INTENTOS UTILIZADOS EN CADA PARTIDA:")
    println("------------------------------------------------------")
    println("Los intentos que utiliza en el problema uno son: ${enunciado1.intentos}")
    println("Los intentos que utiliza en el problema uno son: ${enunciado2.intentos}")
    println("Los intentos que utiliza en el problema uno son: ${enunciado3.intentos}")
    println("Los intentos que utiliza en el problema uno son: ${enunciado4.intentos}")
    println("Los intentos que utiliza en el problema uno son: ${enunciado5.intentos}")
    println("------------------------------------------------------")

    println("NUMEROS UTILIZADOS EN CADA PARTIDA: ")
    println("------------------------------------------------------")
    println("Los numeros utlizados en el PROBLEMA 1 son: ${enunciado1.listaIntentos}")
    println("Los numeros utlizados en el PROBLEMA 2 son: ${enunciado2.listaIntentos}")
    println("Los numeros utlizados en el PROBLEMA 3 son: ${enunciado3.listaIntentos}")
    println("Los numeros utlizados en el PROBLEMA 4 son: ${enunciado4.listaIntentos}")
    println("Los numeros utlizados en el PROBLEMA 5 son: ${enunciado5.listaIntentos}")
    println("------------------------------------------------------")

    println("ESTADISTICAS: ")
    println("------------------------------")
    println("El total de intentos son: ${enunciado1.intentos + enunciado2.intentos + enunciado3.intentos + enunciado4.intentos + enunciado5.intentos}")
    println("------------------------------")
}